package com.radiobros.app2;

/**
 * Created by willem on 03/10/15.
 */
public class Station {

    public Station(String name, String streamUrl, String country, String provinceOrState, String city, String genre) {
        this.name = name;
        this.streamUrl = streamUrl;
        this.country = country;
        this.provinceOrState = provinceOrState;
        this.city = city;
        this.genre = genre;
    }

    public Station(String name, String streamUrl, String country, String provinceOrState, String city, String genre, String logo) {
        this.name = name;
        this.streamUrl = streamUrl;
        this.country = country;
        this.provinceOrState = provinceOrState;
        this.city = city;
        this.genre = genre;
        this.logo = logo;
    }

    public String name;
    public String streamUrl;
    public String country;
    public String provinceOrState;
    public String city;
    public String genre;
    public String logo;

    @Override
    public boolean equals(Object o) {
        return this.name.equals(((Station) o).name);
    }


}
