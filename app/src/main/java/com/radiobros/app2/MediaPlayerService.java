package com.radiobros.app2;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by willem on 11/10/15.
 */
public class MediaPlayerService {

    private static final String TAG = "MediaPlayerService";
    private MediaControlListener.Status status;

    private static MediaPlayer mediaPlayer;

    // START - singleton
    private MediaPlayerService() {
    }

    private static MediaPlayerService mediaPlayerService;

    public static MediaPlayerService getInstance() {
        if (mediaPlayerService == null) mediaPlayerService = new MediaPlayerService();
        return mediaPlayerService;
    }
    // END - singleton


    //Register Observer
    public List<MediaControlListener> mediaControlListeners = new ArrayList<>();

    public void registerMediaControlListener(MediaControlListener mediaControlListener) {
        mediaControlListeners.add(mediaControlListener);
    }

    private void update(MediaControlListener.Status status) {
        this.status = status;
        for (MediaControlListener mcl : mediaControlListeners) {
            mcl.changeListener(status);
        }
    }

    public synchronized MediaPlayer getMediaPlayer() {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        } else {
            mediaPlayer.reset();
        }
        return mediaPlayer;
    }

    public MediaControlListener.Status getStatus() {
        if (mediaPlayer == null) return MediaControlListener.Status.NULL;
        return status;
    }

    private Station station;
    public Station getPlayingStation(){
        return station;
    }

    public void play(Station station) {
        this.station = station;
        update(MediaControlListener.Status.BUFFERING);
        MediaPlayer mediaPlayer0 = getMediaPlayer();
        try {
            mediaPlayer0.setDataSource(station.streamUrl);
            mediaPlayer0.prepareAsync();
            mediaPlayer0.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                    update(MediaControlListener.Status.ONPLAYING);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        if (mediaPlayer == null) return;
        mediaPlayer.pause();
        update(MediaControlListener.Status.ONPAUSE);
    }

    public boolean isPlaying() {
        if (mediaPlayer == null) return false;
        return mediaPlayer.isPlaying();
    }

    public void playFromPause() {
        mediaPlayer.start();
        update(MediaControlListener.Status.ONPLAYING);
    }

    public void stop() {
        if (isPlaying()) {
            mediaPlayer.stop();
        }
        update(MediaControlListener.Status.ONSTOP);
    }

    public MusicIntentReceiver getMuiscIntentRecreiver() {
        return new MusicIntentReceiver();
    }

    public class MusicIntentReceiver extends WakefulBroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
//            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
//                int state = intent.getIntExtra("state", -1);
//                switch (state) {
//                    case 0:
//                        Log.d(TAG, "Headset is unplugged");
//                        pause();
//                        break;
//                    case 1:
//                        Log.d(TAG, "Headset is plugged");
//                        break;
//                    default:
//                        Log.d(TAG, "I have no idea what the headset state is");
//                }
//            }
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                Log.d(TAG, "Audio becoming noisy");
                pause();
            }
            if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        pause();
                        break;
                }
            }
        }
    }

}
