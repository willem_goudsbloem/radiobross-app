package com.radiobros.app2;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

public class RecentFragment extends Fragment {

    final String TAG = "RecentFragment";

    public RecentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MediaPlayerService.getInstance();
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recent, container, false);
        createListView(view);
        MediaButton.getInstance().createMediaButton(view);
        return view;
    }

//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        //Save selected station
//        ListView listView = (ListView) getView().findViewById(R.id.station_list_view);
//        outState.putInt("selectedStation", listView.getSelectedItemPosition());
//        //save button state
//        outState.putInt("mediaPlayerStatus", MediaPlayerService.getInstance().getStatus().getStatus());
//        super.onSaveInstanceState(outState);
//    }

    private void createListView(View view) {
        ArrayList<Station> stations = new ArrayList<>();
        for (Station station : StreamSource.recent()) {
            stations.add(station);
        }
        Context context = getActivity().getApplicationContext();
        StationListAdapter adapter = new StationListAdapter(context, R.id.station_list_view, stations);
        final ListView listView = (ListView) view.findViewById(R.id.station_list_view);
        StationItemListListener stationItemListListener = new StationItemListListener(stations);
        listView.setOnItemClickListener(stationItemListListener);
        listView.setAdapter(adapter);
    }

}
