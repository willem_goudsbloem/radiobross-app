package com.radiobros.app2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by willem on 25/11/15.
 */
public class StationListAdapter extends ArrayAdapter {

    final String TAG = "StationListAdaptor";

    private List<Station> stations;

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param resource The resource ID for a layout file containing a TextView to use when
     *                 instantiating views.
     * @param objects  The objects to represent in the ListView.
     */
    public StationListAdapter(Context context, int resource, List<Station> stations) {
        super(context, resource, stations);
        this.stations = stations;
    }

    /**
     * {@inheritDoc}
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //convertView = super.getView(position, convertView, parent);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.station_list_view, parent, false);
        }
        ImageView imageView = (ImageView) convertView.findViewById(R.id.stationImage);
        if (stations.get(position).logo != null) {
            byte[] image = Base64.decode(stations.get(position).logo, Base64.DEFAULT);
            Bitmap bm = BitmapFactory.decodeByteArray(image, 0, image.length);
            imageView.setImageBitmap(bm);
        }
        TextView stationName = (TextView) convertView.findViewById(R.id.stationName);
        stationName.setText(stations.get(position).name);
        TextView stationCountry = (TextView) convertView.findViewById(R.id.stationCountry);
        stationCountry.setText(stations.get(position).country);
        TextView stationGenre = (TextView) convertView.findViewById(R.id.stationGenre);
        stationGenre.setText(stations.get(position).genre);
        if (MediaPlayerService.getInstance().getPlayingStation()!=null && stations.get(position).equals(MediaPlayerService.getInstance().getPlayingStation())){
          convertView.setBackgroundResource(R.color.itemListSelected);
            stationName.setTextColor(Color.WHITE);
            stationCountry.setTextColor(Color.WHITE);
            stationGenre.setTextColor(Color.WHITE);
        } else {
            stationName.setTextColor(Color.BLACK);
            stationCountry.setTextColor(Color.GRAY);
            stationGenre.setTextColor(Color.GRAY);
        }
        return convertView;
    }

}
