package com.radiobros.app2;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SearchFragment extends Fragment {

    final static String TAG = "SearchFragment";

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        createSearchList(view);
        searchInput(view);
        MediaButton.getInstance().createMediaButton(view);
        return view;
    }

    private void searchInput(View view) {
        EditText search = (EditText) view.findViewById(R.id.searchInput);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            // http://radiobros-api.appspot.com/station?q=
            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                //only care if more than x characters are typed from blank
                if (s.length() < 3 && before == 0) return;
                AsyncTask<String, Void, Station[]> asyncTask = new AsyncTask<String, Void, Station[]>() {
                    @Override
                    protected Station[] doInBackground(String... params) {
                        //we only search after more than x characters are typed
                        if (s.length() > 2) {
                            try {
                                URL url = new URL("http://radiobros-api.appspot.com/station?q=" + URLEncoder.encode(s.toString(), "UTF-8"));
                                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                InputStream in = connection.getInputStream();
                                Gson gson = new Gson();
                                Reader reader = new InputStreamReader(in);
                                return gson.fromJson(reader, Station[].class);
                            } catch (Exception e) {
                                Log.e(TAG, "", e);
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Station[] _stations) {
                        super.onPostExecute(_stations);
                        adapter.clear();
                        //if station is null there is no result or there are characters deleted
                        if (_stations != null) {
                            stations = Arrays.asList(_stations);
                            adapter.addAll(stations);
                        }
                        adapter.notifyDataSetChanged();
                    }
                };
                asyncTask.execute();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    static List<Station> stations;
    static ArrayAdapter adapter;

    private void createSearchList(View view) {
        stations = new ArrayList<>();
        Context context = getActivity().getApplicationContext();
        adapter = new StationListAdapter(context, R.id.station_list_view, stations);
        final ListView listView = (ListView) view.findViewById(R.id.station_list_view);
        StationItemListListener stationItemListListener = new StationItemListListener(stations);
        listView.setOnItemClickListener(stationItemListListener);
        listView.setAdapter(adapter);
    }

}
