package com.radiobros.app2;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends FragmentActivity {

    private MediaPlayerService.MusicIntentReceiver musicIntentReceiver = MediaPlayerService.getInstance().getMuiscIntentRecreiver();
    private Fragment recentFragment, searchFragment;

    private void notification(boolean display) {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(!display) {
            mNotificationManager.cancelAll();
            return;
        }
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notify_play_circle)
                        .setContentTitle("Radiobros")
                        .setContentText("Radio is playing...");
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        // the 99 below should be an identifier, which allows you to update the notification later on.
        mNotificationManager.notify(99, mBuilder.build());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment_container);
        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                recentFragment = getSupportFragmentManager().getFragment(savedInstanceState, "recentFragment");
                searchFragment = getSupportFragmentManager().getFragment(savedInstanceState, "searchFragment");
                return;
            }
            recentFragment = new RecentFragment();
            searchFragment = new SearchFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, recentFragment).commit();
            notificationListener();
        }
    }

    public void notificationListener(){
        MediaPlayerService.getInstance().registerMediaControlListener(new MediaControlListener() {
            @Override
            public void changeListener(Status status) {
                switch (status) {
                    case ONPLAYING:
                        notification(true);
                        break;
                    case ONPAUSE:
                        notification(false);
                        break;
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        getSupportFragmentManager().putFragment(outState, "recentFragment", recentFragment);
        getSupportFragmentManager().putFragment(outState, "searchFragment", searchFragment);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(musicIntentReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(musicIntentReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (id) {
            case R.id.action_search:
                ft.replace(R.id.fragment_container, searchFragment);
                ft.addToBackStack("action_search");
                ft.commit();
                break;
            case R.id.action_recent:
                ft.replace(R.id.fragment_container, recentFragment);
                ft.addToBackStack("action_recent");
                ft.commit();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}
