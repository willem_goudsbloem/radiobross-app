package com.radiobros.app2;

import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by willem on 23/09/15.
 */
public class StationItemListListener implements AdapterView.OnItemClickListener {

    private List<Station> stations;

    private StationItemListListener() {
    }

    public StationItemListListener(List<Station> stations) {
        this.stations = stations;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        GridLayout gridLayout = (GridLayout) view;
        // Clear styling of previous selected items
        for (int i = 0; i < parent.getChildCount(); i++) {
            parent.getChildAt(i).setBackgroundColor(view.getDrawingCacheBackgroundColor());
            ((TextView) parent.getChildAt(i).findViewById(R.id.stationName)).setTextColor(Color.BLACK);
            ((TextView) parent.getChildAt(i).findViewById(R.id.stationCountry)).setTextColor(Color.GRAY);
            ((TextView) parent.getChildAt(i).findViewById(R.id.stationGenre)).setTextColor(Color.GRAY);
        }
        MediaPlayerService.getInstance().stop();
        // Style selected item
        gridLayout.setBackgroundResource(R.color.itemListSelected);
        ((TextView) gridLayout.findViewById(R.id.stationName)).setTextColor(Color.WHITE);
        ((TextView) gridLayout.findViewById(R.id.stationCountry)).setTextColor(Color.WHITE);
        ((TextView) gridLayout.findViewById(R.id.stationGenre)).setTextColor(Color.WHITE);
        //play the stream as selected
        MediaPlayerService.getInstance().play(stations.get(position));
    }


}