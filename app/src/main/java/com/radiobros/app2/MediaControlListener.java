package com.radiobros.app2;

/**
 * Created by willem on 12/10/15.
 */
public interface MediaControlListener {


    enum Status {
        NULL(0), ONPLAYING(1), ONPAUSE(2), ONSTOP(3), BUFFERING(4);
        private int id;

        Status(int id) {
            this.id = id;
        }

        public int getStatus() {
            return id;
        }

        static Status fromValue(int id) {
            return Status.values()[0];
        }

    }

    void changeListener(Status status);

}
