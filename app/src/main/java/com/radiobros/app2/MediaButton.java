package com.radiobros.app2;

import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

/**
 * Created by willem on 23/09/15.
 */
public class MediaButton implements View.OnClickListener, MediaControlListener {

    private ImageButton mediaCtrlBtn;

    private MediaButton(){
    }
    private static MediaButton mediaButton = new MediaButton();
    public static MediaButton getInstance(){
        return mediaButton;
    }

    public void createMediaButton(View view) {
        mediaCtrlBtn = (ImageButton) view.findViewById(R.id.mediaCtrlBtn);
        mediaCtrlBtn.setOnClickListener(this);
        mediaCtrlBtn.setVisibility(View.INVISIBLE);
        MediaPlayerService.getInstance().registerMediaControlListener(this);
        //when this fragment is reloaded
        //todo: find cleaner way?
        if(MediaPlayerService.getInstance().getStatus() != null) {
            changeListener(MediaPlayerService.getInstance().getStatus());
        }
        manageProgressBar(view);
    }

    private void manageProgressBar(View view) {
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        MediaPlayerService.getInstance().registerMediaControlListener(new MediaControlListener() {
            @Override
            public void changeListener(Status status) {
                switch (status) {
                    case BUFFERING:
                        progressBar.setVisibility(ProgressBar.VISIBLE);
                        break;
                    default:
                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (MediaPlayerService.getInstance().isPlaying()) {
            MediaPlayerService.getInstance().pause();
        } else {
            MediaPlayerService.getInstance().playFromPause();
        }
    }

    @Override
    public void changeListener(Status status) {
        Log.d("MediaButton", "status:"+status);
        switch (status){
            case ONPAUSE:
                mediaCtrlBtn.setImageResource(R.drawable.play_circle);
                mediaCtrlBtn.setVisibility(View.VISIBLE);
                break;
            case ONPLAYING:
                mediaCtrlBtn.setImageResource(R.drawable.pause_circle);
                mediaCtrlBtn.setVisibility(View.VISIBLE);
                break;
            case ONSTOP:
                mediaCtrlBtn.setVisibility(View.INVISIBLE);
                break;
            case BUFFERING:
                mediaCtrlBtn.setVisibility(View.INVISIBLE);
                break;
        }
    }



}
